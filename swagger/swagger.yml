info:
  title: Comentario
  version: 1.0.0
consumes:
  - application/json
produces:
  - application/json
schemes:
  - http
swagger: "2.0"
basePath: /api

definitions:

  apiResponseBase:
    description: Base API response
    type: object
    readOnly: true
    properties:
      success:
        type: boolean
        x-omitempty: false
      message:
        type: string

  comment:
    type: object
    properties:
      commentHex:
        $ref: "#/definitions/hexId"
      commenterHex:
        $ref: "#/definitions/commenterHexId"
      parentHex:
        $ref: "#/definitions/parentHexId"
      domain:
        type: string
      creationDate:
        type: string
        format: date-time
      state:
        $ref: "#/definitions/commentState"
      deleted:
        type: boolean
        x-omitempty: false
      direction:
        type: integer
        x-omitempty: false
      score:
        type: integer
        x-omitempty: false
      markdown:
        type: string
      html:
        type: string
      url:
        type: string

  commenter:
    type: object
    properties:
      commenterHex:
        $ref: "#/definitions/commenterHexId"
      email:
        type: string
        format: email
      name:
        type: string
      link:
        type: string
      photo:
        type: string
      provider:
        type: string
      joinDate:
        type: string
      isModerator:
        type: boolean
        x-omitempty: false

  commenterHexId:
    description: Commenter hex ID/token, which can also be 'anonymous' when commenting anonymously
    type: string
    maxLength: 64
    pattern: 'anonymous|[0-9a-f]{64}'

  commentState:
    description: Comment state
    type: string
    enum:
      - approved
      - unapproved
      - flagged

  domain:
    description: Registered domain
    type: object
    properties:
      domain:
        type: string
      ownerHex:
        $ref: "#/definitions/hexId"
      name:
        type: string
      creationDate:
        type: string
        format: date-time
      state:
        $ref: "#/definitions/domainState"
      importedComments:
        type: boolean
        x-omitempty: false
      autoSpamFilter:
        type: boolean
        x-omitempty: false
      requireModeration:
        type: boolean
        x-omitempty: false
      requireIdentification:
        type: boolean
        x-omitempty: false
      moderateAllAnonymous:
        type: boolean
        x-omitempty: false
      moderators:
        type: array
        items:
          $ref: "#/definitions/domainModerator"
      emailNotificationPolicy:
        $ref: "#/definitions/emailNotificationPolicy"
      idps:
        $ref: "#/definitions/idpMap"
      ssoSecret:
        type: string
      ssoUrl:
        type: string
        format: url
      defaultSortPolicy:
        $ref: "#/definitions/sortPolicy"

  domainModerator:
    description: Domain moderator
    type: object
    properties:
      email:
        type: string
        format: email
      domain:
        type: string
      addDate:
        type: string
        format: date-time

  domainState:
    description: Domain state
    type: string
    enum:
      - unfrozen
      - frozen

  email:
    type: object
    properties:
      email:
        type: string
        format: email
      unsubscribeSecretHex:
        $ref: "#/definitions/hexId"
      lastEmailNotificationDate:
        type: string
      sendReplyNotifications:
        type: boolean
        x-omitempty: false
      sendModeratorNotifications:
        type: boolean
        x-omitempty: false

  entity:
    description: Entity for resetting the password
    type: string
    enum:
      - owner
      - commenter

  emailNotificationPolicy:
    description: Email notification policy
    type: string
    enum:
      - all
      - none
      - pending-moderation

  hexId:
    description: ID consisting of 64 hex digits
    type: string
    minLength: 64
    maxLength: 64
    pattern: '[0-9a-f]{64}'

  idpMap:
    description: Map of enabled identity providers (name => boolean), including 'commento', 'sso', and all known federated IdPs
    type: object
    x-go-type:
      import:
        package: "gitlab.com/comentario/comentario/internal/api/exmodels"
      type: "IdentityProviderMap"
    x-omitempty: false

  owner:
    description: Instance owner
    type: object
    properties:
      ownerHex:
        $ref: "#/definitions/hexId"
      email:
        type: string
        format: email
      name:
        type: string
      confirmedEmail:
        type: boolean
      joinDate:
        type: string
        format: date-time

  page:
    description: Page hosting comments
    type: object
    properties:
      domain:
        type: string
      path:
        type: string
      isLocked:
        type: boolean
      commentCount:
        type: integer
      stickyCommentHex:
        type: string
      title:
        type: string

  parentHexId:
    description: ID similar to HexID, consisting of 64 hex digits, which can also be 'root'
    type: string
    maxLength: 64
    pattern: 'root|[0-9a-f]{64}'

  sortPolicy:
    description: Sort policy
    type: string
    enum:
      - score-desc
      - creationdate-desc
      - creationdate-asc

parameters:

  federatedIdpId:
    in: path
    name: provider
    required: true
    description: Federated identity provider ID
    type: string
    enum:
      - github
      - gitlab
      - google
      - twitter

responses:

  # 307
  OAuthRedirect:
    description: Redirecting to a federated identity provider
    headers:
      Location:
        type: string

  # 401
  OAuthFailure:
    description: Authentication failed
    schema:
      type: string

paths:
  /_:
    options:
      operationId: Generic
      summary: Fake endpoint to provide generic errors
      responses:
        400:
          description: Bad request
          schema:
            type: object
            properties:
              details:
                type: string
        401:
          description: Unauthorized
          schema:
            type: object
            properties:
              details:
                type: string
        404:
          description: Resource not found
          schema:
            type: object
            properties:
              details:
                type: string
        500:
          description: Internal server error
          schema:
            type: object
            properties:
              details:
                type: string

  #---------------------------------------------------------------------------------------------------------------------
  # Auth
  #---------------------------------------------------------------------------------------------------------------------

  /forgot:
    post:
      operationId: ForgotPassword
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - email
              - entity
            properties:
              email:
                type: string
                format: email
              entity:
                $ref: "#/definitions/entity"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /reset:
    post:
      operationId: ResetPassword
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - resetHex
              - password
            properties:
              resetHex:
                $ref: "#/definitions/hexId"
              password:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              entity:
                $ref: "#/definitions/entity"

  #---------------------------------------------------------------------------------------------------------------------
  # Comments
  #---------------------------------------------------------------------------------------------------------------------

  /comment/approve:
    post:
      operationId: CommentApprove
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - commentHex
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              commentHex:
                $ref: "#/definitions/hexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /comment/count:
    post:
      operationId: CommentCount
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - domain
              - paths
            properties:
              domain:
                type: string
              paths:
                type: array
                items:
                  type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              commentCounts:
                type: object # map[string]int

  /comment/delete:
    post:
      operationId: CommentDelete
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - commentHex
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              commentHex:
                $ref: "#/definitions/hexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /comment/edit:
    post:
      operationId: CommentEdit
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - commentHex
              - markdown
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              commentHex:
                $ref: "#/definitions/hexId"
              markdown:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              html:
                type: string

  /comment/list:
    post:
      operationId: CommentList
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - domain
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              domain:
                type: string
                minLength: 1
              path:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              domain:
                type: string
              comments:
                type: array
                items:
                  $ref: "#/definitions/comment"
              commenters:
                type: object # map[string]commenter
              requireModeration:
                type: boolean
              requireIdentification:
                type: boolean
              isFrozen:
                type: boolean
              isModerator:
                type: boolean
              defaultSortPolicy:
                $ref: "#/definitions/sortPolicy"
              attributes:
                $ref: "#/definitions/page"
              configuredOauths:
                $ref: "#/definitions/idpMap"

  /comment/new:
    post:
      operationId: CommentNew
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - domain
              - parentHex
              - markdown
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              domain:
                type: string
              path:
                type: string
              parentHex:
                $ref: "#/definitions/parentHexId"
              markdown:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              commentHex:
                $ref: "#/definitions/hexId"
              html:
                type: string
              state:
                $ref: "#/definitions/commentState"

  /comment/vote:
    post:
      operationId: CommentVote
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - commentHex
              - direction
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              commentHex:
                $ref: "#/definitions/hexId"
              direction:
                type: integer
                enum:
                  - -1
                  - 0
                  - 1
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  #---------------------------------------------------------------------------------------------------------------------
  # Commenters
  #---------------------------------------------------------------------------------------------------------------------

  /commenter/login:
    post:
      operationId: CommenterLogin
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - email
              - password
            properties:
              email:
                type: string
                format: email
              password:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              commenter:
                $ref: "#/definitions/commenter"
              email:
                $ref: "#/definitions/email"

  /commenter/new:
    post:
      operationId: CommenterNew
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - name
              - email
              - password
            properties:
              name:
                type: string
              website:
                type: string
              email:
                type: string
                format: email
              password:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              confirmEmail:
                type: boolean

  /commenter/photo:
    get:
      operationId: CommenterPhoto
      produces:
        - image/jpeg
      parameters:
        - name: commenterHex
          in: query
          type: string
          required: true
          maxLength: 64
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: file

  /commenter/self:
    post:
      operationId: CommenterSelf
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              commenter:
                $ref: "#/definitions/commenter"
              email:
                $ref: "#/definitions/email"

  /commenter/token/new:
    get:
      operationId: CommenterTokenNew
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              commenterToken:
                $ref: "#/definitions/commenterHexId"

  /commenter/update:
    post:
      operationId: CommenterUpdate
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - name
              - email
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              name:
                type: string
              email:
                type: string
                format: email
              link:
                type: string
              photo:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  #---------------------------------------------------------------------------------------------------------------------
  # Domains
  #---------------------------------------------------------------------------------------------------------------------

  /domain/clear:
    post:
      operationId: DomainClear
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /domain/delete:
    post:
      operationId: DomainDelete
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /domain/export/begin:
    post:
      operationId: DomainExportBegin
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /domain/export/download:
    get:
      operationId: DomainExportDownload
      produces:
        - application/gzip
      parameters:
        - in: query
          name: exportHex
          type: string
          required: true
          minLength: 64
          maxLength: 64
      responses:
        200:
          description: Export file
          schema:
            type: file
          headers:
            Content-Disposition:
              type: string

  /domain/import/commento:
    post:
      operationId: DomainImportCommento
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
              - url
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
              url:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              numImported:
                type: integer

  /domain/import/disqus:
    post:
      operationId: DomainImportDisqus
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
              - url
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
              url:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              numImported:
                type: integer

  /domain/list:
    post:
      operationId: DomainList
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              domains:
                type: array
                items:
                  $ref: "#/definitions/domain"
              configuredOauths:
                $ref: "#/definitions/idpMap"

  /domain/moderator/delete:
    post:
      operationId: DomainModeratorDelete
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
              - email
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
              email:
                type: string
                format: email
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /domain/moderator/new:
    post:
      operationId: DomainModeratorNew
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
              - email
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
              email:
                type: string
                format: email
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /domain/new:
    post:
      operationId: DomainNew
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - name
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              name:
                type: string
                minLength: 1
              domain:
                type: string
                minLength: 1
                maxLength: 253 # Maximum length of a valid DNS entry
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              domain:
                type: string

  /domain/sso/new:
    post:
      operationId: DomainSsoSecretNew
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              ssoSecret:
                $ref: "#/definitions/hexId"

  /domain/statistics:
    post:
      operationId: DomainStatistics
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              viewsLast30Days:
                type: array
                items:
                  type: integer
              commentsLast30Days:
                type: array
                items:
                  type: integer

  /domain/update:
    post:
      operationId: DomainUpdate
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
              - domain
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
              domain:
                $ref: "#/definitions/domain"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  #---------------------------------------------------------------------------------------------------------------------
  # Emails
  #---------------------------------------------------------------------------------------------------------------------

  /email/get:
    post:
      operationId: EmailGet
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - unsubscribeSecretHex
            properties:
              unsubscribeSecretHex:
                $ref: "#/definitions/hexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              email:
                $ref: "#/definitions/email"

  /email/moderate:
    get:
      operationId: EmailModerate
      parameters:
        - name: unsubscribeSecretHex
          in: query
          type: string
          required: true
          minLength: 64
          maxLength: 64
        - name: action
          in: query
          type: string
          required: true
          enum:
            - approve
            - delete
        - name: commentHex
          in: query
          type: string
          required: true
          minLength: 64
          maxLength: 64
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  /email/update:
    post:
      operationId: EmailUpdate
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - email
            properties:
              email:
                $ref: "#/definitions/email"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  #---------------------------------------------------------------------------------------------------------------------
  # Owners
  #---------------------------------------------------------------------------------------------------------------------

  /owner/confirm-hex:
    get:
      operationId: OwnerConfirmHex
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - in: formData
          name: confirmHex
          required: true
          type: string
          minLength: 64
          maxLength: 64
      responses:
        307:
          description: Redirect to login
          headers:
            Location:
              type: string

  /owner/new:
    post:
      operationId: OwnerNew
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - name
              - email
              - password
            properties:
              name:
                type: string
              email:
                type: string
                format: email
              password:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              confirmEmail:
                type: boolean

  /owner/login:
    post:
      operationId: OwnerLogin
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - email
              - password
            properties:
              email:
                type: string
                format: email
              password:
                type: string
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              ownerToken:
                $ref: "#/definitions/hexId"

  /owner/self:
    post:
      operationId: OwnerSelf
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            type: object
            properties:
              success:
                type: boolean
                x-omitempty: false
              message:
                type: string
              loggedIn:
                type: boolean
                x-omitempty: false
              owner:
                $ref: "#/definitions/owner"

  /owner/delete:
    post:
      operationId: OwnerDelete
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - ownerToken
            properties:
              ownerToken:
                $ref: "#/definitions/hexId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  #---------------------------------------------------------------------------------------------------------------------
  # Pages
  #---------------------------------------------------------------------------------------------------------------------

  /page/update:
    post:
      operationId: PageUpdate
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - commenterToken
              - domain
              - attributes
            properties:
              commenterToken:
                $ref: "#/definitions/commenterHexId"
              domain:
                type: string
              path:
                type: string
              attributes:
                $ref: "#/definitions/page"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"

  #---------------------------------------------------------------------------------------------------------------------
  # OAuth
  #---------------------------------------------------------------------------------------------------------------------

  /oauth/{provider}:
    get:
      operationId: OauthInit
      summary: Initiate signing in using federated authentication
      produces:
        - text/html # For an error response only, otherwise a redirect is issued
      parameters:
        - $ref: "#/parameters/federatedIdpId"
        - in: query
          name: commenterToken
          type: string
          required: true
      responses:
        307:
          $ref: "#/responses/OAuthRedirect"
        401:
          $ref: "#/responses/OAuthFailure"

  /oauth/{provider}/callback:
    get:
      operationId: OauthCallback
      summary: Endpoint that gets called back by the federated authentication provider
      produces:
        - text/html
      parameters:
        - $ref: "#/parameters/federatedIdpId"
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"
        401:
          $ref: "#/responses/OAuthFailure"

  /oauth/sso/callback:
    get:
      operationId: OauthSsoCallback
      produces:
        - text/html
      parameters:
        - in: query
          name: payload
          type: string
          required: true
        - in: query
          name: hmac
          type: string
          required: true
      responses:
        200:
          description: Success or failure response (see 'success' property)
          schema:
            $ref: "#/definitions/apiResponseBase"
        401:
          $ref: "#/responses/OAuthFailure"

  /oauth/sso/redirect:
    get:
      operationId: OauthSsoRedirect
      produces:
        - text/html # For an error response only, otherwise a redirect is issued
      parameters:
        - in: query
          name: commenterToken
          type: string
          required: true
      responses:
        307:
          $ref: "#/responses/OAuthRedirect"
        401:
          $ref: "#/responses/OAuthFailure"

  #---------------------------------------------------------------------------------------------------------------------
  # Testing endpoints
  # These endpoints are only available during the test cycle and never in production
  #---------------------------------------------------------------------------------------------------------------------

  /e2e/reset:
    post:
      operationId: E2eReset
      summary: Reset the backend (all settings and database) to testing defaults
      security: []
      responses:
        204:
          description: The backend has been successfully reset
